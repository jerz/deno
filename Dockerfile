FROM --platform=amd64 debian:stable-slim AS download

ARG DENO_VERSION=v1.11.0

RUN apt-get update -yqq && apt-get install -yqq wget unzip

WORKDIR /download
RUN wget https://github.com/denoland/deno/releases/download/${DENO_VERSION}/deno-x86_64-unknown-linux-gnu.zip && \
    unzip deno-x86_64-unknown-linux-gnu.zip

FROM --platform=amd64 debian:stable-slim

COPY --from=download /download/deno /usr/bin/deno
